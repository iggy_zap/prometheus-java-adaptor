Why?
---------

This project exists as alternative to official prometheus java project since there are few problems with it:

1. Code based on java 5/6 and implementation being too eager to collect metrics on scrape. That results in potential 
    explosiveness of scrape (it has to be rate-limited on client side), slowness and unfriendliness for garbage collector.
    
1. Original API design does not allow metrics with same name but different type, that will force Prometheus to discard 
    such data (or part of the series). Java client library design does nothing to address this problem.
    
1. Performance: when scrape is performed quite often metric names and labels are re-generated, which adds extra runtime 
    cost. Given that amount of metric(s) is constant (growing from 0 at the start of JVM to fixed number when software is configured 
    and ready to be interacted with) or should be constant over time in well-written system, that gives a way to reduce 
    cost of producing scrape by pre-computing certain values in _advance_ of scrape
    
1.  Performance: Collection of metrics can be done in 2 ways - quering for all registered metrics and then pre-computing common data
    or event-driven - pre-computing common data when new metric is registered. First method requires certainty that there is 
    exactly 1 metric within the system with specific name (this example uses JMX)

1.  Pluggability: This project focuses on reporting side of the problem, not on collecting side of the problem, hence 
    it needs to provide easy way to plug in existing metrics libraries (JMX, Dropwizard Metrics, etc)
 

License
----------
This library is covered by GNU GPL v3. 
Please contact me if you want a different license.