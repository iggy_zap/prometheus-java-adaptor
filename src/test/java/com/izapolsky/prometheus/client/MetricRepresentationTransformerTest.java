package com.izapolsky.prometheus.client;

import org.apache.commons.io.IOUtils;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

import static com.izapolsky.prometheus.client.MetricRepresentationTransformer.EXPOSITION_CHARSET;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class MetricRepresentationTransformerTest {

    private MetricWriteTransormer toTest = new MetricRepresentationTransformer();

    @Mock
    private MetricRepresentation mockMetric;

    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Test
    public void testSimpleMetricContainsHelpAnd() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        long timestamp = System.currentTimeMillis();

        when(mockMetric.name()).thenReturn("foo.bar.baz");
        when(mockMetric.help()).thenReturn(Optional.of("Sample metric"));
        when(mockMetric.representationType()).thenReturn(MetricRepresentation.Type.COUNTER);
        when(mockMetric.lazyValues()).thenReturn(Collections.singletonList(
                getValue(timestamp, 1.0,
                        MetricRepresentation.ValueType.LITERAL,
                        Arrays.asList(new MetricRepresentation.LabelValue(new MetricRepresentation.Label("x"), "y")))));

        for (MetricWriteTransormer.WriteInfo wr : toTest.bytes(mockMetric)) {
            baos.write(wr.what, wr.position, wr.length);
        }
        baos.close();

        assertEquals(Arrays.asList("# TYPE foo.bar.baz counter", "# HELP foo.bar.baz Sample metric", "foo.bar.baz { x=\"y\" } 1.0 " + timestamp),
                IOUtils.readLines(new ByteArrayInputStream(baos.toByteArray()), EXPOSITION_CHARSET));

    }

    @Test
    public void testSanitisesDimensionsValue() throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        when(mockMetric.name()).thenReturn("foo.bar.baz");
        when(mockMetric.help()).thenReturn(Optional.empty());
        when(mockMetric.representationType()).thenReturn(MetricRepresentation.Type.COUNTER);
        when(mockMetric.lazyValues()).thenReturn(Collections.singletonList(
                getValue( Double.NaN,
                        MetricRepresentation.ValueType.LITERAL,
                        Arrays.asList(new MetricRepresentation.LabelValue(new MetricRepresentation.Label("x"), "y=\\\"\n")))));

        for (MetricWriteTransormer.WriteInfo wr : toTest.bytes(mockMetric)) {
            baos.write(wr.what, wr.position, wr.length);
        }
        baos.close();

        assertEquals(Arrays.asList("# TYPE foo.bar.baz counter", "foo.bar.baz { x=\"y=\\\\\\\"\\n\" } Nan"),
                IOUtils.readLines(new ByteArrayInputStream(baos.toByteArray()), EXPOSITION_CHARSET));
    }

    private SortedSet<MetricRepresentation.LabelValue> getDimensions(Collection<MetricRepresentation.LabelValue> values){
        return new TreeSet<>(values);
    }

    private MetricRepresentation.Value getValue(double value, MetricRepresentation.ValueType type, Collection<MetricRepresentation.LabelValue> dimensions) {
        return new MetricRepresentation.Value(OptionalLong.empty(), value, getDimensions(dimensions), type);
    }

    private MetricRepresentation.Value getValue(long timestamp, double value, MetricRepresentation.ValueType type, Collection<MetricRepresentation.LabelValue> dimensions) {
        return new MetricRepresentation.Value(OptionalLong.of(timestamp), value, getDimensions(dimensions), type);
    }

    private MetricRepresentation.Value getValue(long timestamp, double value, MetricRepresentation.ValueType type) {
        return getValue(timestamp, value, type, Collections.emptyList());
    }


}