/*
This software is designed to act as a prometheus exposition bridge for Java
        Copyright (C) 2018  Ignat Zapolsky

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.izapolsky.prometheus.client;

public interface MetricWriteTransormer {
    Iterable<? extends WriteInfo> bytes(MetricRepresentation representation);

    public static class WriteInfo {
        public final byte[] what;
        public final int length;
        public final int position;

        WriteInfo(byte[] what, int length, int position) {
            this.what = what;
            this.length = length;
            this.position = position;
        }

        public WriteInfo(byte[] bytes) {
            this (bytes, bytes.length, 0);
        }
    }
}
