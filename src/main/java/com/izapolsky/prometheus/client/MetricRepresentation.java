/*
This software is designed to act as a prometheus exposition bridge for Java
        Copyright (C) 2018  Ignat Zapolsky

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.izapolsky.prometheus.client;

import java.util.*;

/**
 * This interface defines contract for representing metrics to prometheus.
 */
public interface MetricRepresentation {

    /**
     * Name of the representation. Have a requirement to be compatible with PromQL which has no requirements.
     * Prometheus guys tend to use underscore to separate metric parts and utilise lower-case words a lot.
     * Most likely their parser will fail on quoted line-feed strings.
     *
     * @return metric name
     */
    String name();

    /**
     * A helpful description of the metric.
     *
     * @return Optional description
     */
    Optional<String> help();

    /**
     * A metric exposition is one of 4 defined types, each denotes some fixed labels and additional values
     *
     * @return
     */
    Type representationType();

    /**
     * A way to query current values in generic and lazy way - only when we create iterator, we need to start gathering values
     *
     * @return
     */
    Iterable<Value> lazyValues();

    /**
     * Each type also can have a number of supplied labels, which are name+value pairs. This normally adds extra dimensions.
     * In addition, some types will provide extra labels (see summary and histogram) and additional series.
     */
    public enum Type {
        // a gauge provides just an informative value
        GAUGE(EnumSet.of(ValueType.LITERAL)),
        // a counter provides value that monotonically increases in time. it can go to 0 when service restarts
        COUNTER(EnumSet.of(ValueType.LITERAL)),
        // a metric that _samples_ observations, it provides :
        // streaming φ-quantiles (0 ≤ φ ≤ 1) of observed events, exposed as <basename>{quantile="<φ>"}
        // the total sum of all observed values, exposed as <basename>_sum
        // the count of events that have been observed, exposed as <basename>_count
        SUMMARY(EnumSet.of(ValueType.LITERAL, ValueType.SUM, ValueType.COUNT)),
        //a metric that _samples_ observations, it provides :
        // cumulative counters for the observation buckets, exposed as <basename>_bucket{le="<upper inclusive bound>"}
        // the total sum of all observed values, exposed as <basename>_sum
        // the count of events that have been observed, exposed as <basename>_count (identical to <basename>_bucket{le="+Inf"} above)
        HISTOGRAM(EnumSet.of(ValueType.BUCKET, ValueType.SUM, ValueType.COUNT));

        public final EnumSet<ValueType> allowedValues;

        Type(EnumSet<ValueType> allowedValues) {
            this.allowedValues = allowedValues;
        }
    }

    /**
     * This enum represents additional semantics about values that can be supplied by metric. This is linked to required
     * representation from Type enum
     */
    public enum ValueType {
        LITERAL,
        COUNT,
        SUM,
        BUCKET
    }

    // a value class to represent response
    public static class Value {
        public final OptionalLong timestamp;
        public final double value;
        //it's implied that labels are sorted
        public final SortedSet<LabelValue> dimensions;
        public final ValueType type;

        public Value(OptionalLong timestamp, double value, SortedSet<LabelValue> dimensions, ValueType type) {
            this.timestamp = timestamp;
            this.value = value;
            this.dimensions = dimensions == null ? Collections.emptySortedSet() : Collections.unmodifiableSortedSet(dimensions);
            this.type = type;
        }
    }

    //Represents labeled dimension for metric
    public static class LabelValue implements Comparable<LabelValue> {
        public final Label header;
        public final String value;

        public LabelValue(Label header, String value) {
            this.header = header;
            this.value = value;
        }

        @Override
        public int compareTo(LabelValue o) {
            return header.name.compareTo(o.header.name);
        }

    }

    // a name, or label for dimension
    // there is notion that these labels have to be pre-defined when specific metric representation is created
    public static class Label {
        public final String name;

        public Label(String name) {
            this.name = name;
        }
    }
}
