/*
This software is designed to act as a prometheus exposition bridge for Java
        Copyright (C) 2018  Ignat Zapolsky

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.izapolsky.prometheus.client;

import com.google.common.collect.Iterators;

import java.nio.charset.Charset;
import java.util.*;

/**
 * This class provides an utility method to transform a signular metric representation in a stream (or iterable) of writeale
 * byte arrays
 */
public final class MetricRepresentationTransformer implements MetricWriteTransormer {

    @Override
    public Iterable<? extends WriteInfo> bytes(MetricRepresentation representation) {

        return () -> new ExpandingIterator<>(
                Arrays.asList(Generator.values()).iterator(),
                (vl, notUsed) -> vl.infos(representation));
    }

    public final static Charset EXPOSITION_CHARSET;

    static {
        EXPOSITION_CHARSET = Charset.forName("UTF-8");
    }

    /**
     * A no-op method at the moment
     *
     */
    private static String safeName(String unsafeName) {

        //there is no point in sanitising metric name here
        return unsafeName;
    }

    /**
     * Quotes unsafe characters for help, no op for now
     *
     * @param unsafeHelp
     * @return
     */
    private static String escapeHelp(String unsafeHelp) {
        //help string can be sanitised.
        return unsafeHelp;
    }

    /**
     * We have escape some stuff in label values
     * @param unsafeLabelValue
     * @return
     */
    public static String escapeLabelValue(String unsafeLabelValue) {
        StringBuilder sb = new StringBuilder();
        charloop:for (char c : unsafeLabelValue.toCharArray()) {
            switch (c) {
                case '"':
                case '\\':
                    sb.append('\\');
                    break;
                case '\n':
                    sb.append("\\n");
                    continue charloop;
            }
            sb.append(c);
        }
        return sb.toString();
    }

    enum Generator {
        TYPE {
            @Override
            Iterator<WriteInfo> infos(MetricRepresentation representation) {

                //todo following data is static in nature, so we can effectively cache it.
                final byte[] arr;
                {
                    String text = "# TYPE " + safeName(representation.name()) + " " + representation.representationType().toString().toLowerCase() + "\n";
                    arr = text.getBytes(EXPOSITION_CHARSET);
                }
                return Iterators.singletonIterator(new WriteInfo(arr));
            }
        },
        HELP {
            @Override
            Iterator<WriteInfo> infos(MetricRepresentation representation) {
                final byte[] arr;
                {
                    if (representation.help().isPresent()) {
                        String text = "# HELP " + safeName(representation.name()) + " " + escapeHelp(representation.help().get()) + "\n";
                        arr = text.getBytes(EXPOSITION_CHARSET);
                    } else {
                        arr = null;
                    }
                }
                return arr == null ? Collections.emptyIterator() : Iterators.singletonIterator(new WriteInfo(arr, arr.length, 0));
            }
        },
        DATA {
            @Override
            Iterator<WriteInfo> infos(MetricRepresentation representation) {
                return new ExpandingIterator<>(
                        representation.lazyValues().iterator(),
                        (vl, notUsed) -> asWriteLine(representation, vl));
            }

            private Iterator<WriteInfo> asWriteLine(MetricRepresentation context, MetricRepresentation.Value next) {
                return new ExpandingIterator<>(
                        Arrays.asList(ValueLineSerialiser.values()).iterator(),
                        (vl, notUsed) -> vl.serialise(context, next)
                );
            }
        };

        abstract Iterator<WriteInfo> infos(MetricRepresentation representation);
    }

    /**
     * Enum that is used to serialise value line
     */
    enum ValueLineSerialiser {
        NAME {
            @Override
            public Iterator<WriteInfo> serialise(MetricRepresentation context, MetricRepresentation.Value value) {
                return Iterators.singletonIterator(new WriteInfo(safeName(context.name()).getBytes(EXPOSITION_CHARSET)));
            }
        },
        TYPE_POSTFIX {
            private final WriteInfo TYPE_PREFIX = new WriteInfo(new byte[]{'_'});

            @Override
            public Iterator<WriteInfo> serialise(MetricRepresentation context, MetricRepresentation.Value value) {
                //this can be done differently. we can cache byte representation of each type in enum, and return a singleton iterator from
                // exposition lookup
                return value.type == MetricRepresentation.ValueType.LITERAL ?
                        Collections.emptyIterator() :
                        Arrays.asList(TYPE_PREFIX, new WriteInfo(value.type.name().toLowerCase().getBytes(EXPOSITION_CHARSET))).iterator();
            }
        },
        DIMENSIONS_START {
            private final WriteInfo DIMENSIONS_START = new WriteInfo(new byte[]{' ', '{', ' '});

            @Override
            public Iterator<WriteInfo> serialise(MetricRepresentation context, MetricRepresentation.Value value) {
                return Iterators.singletonIterator(DIMENSIONS_START);
            }
        },
        DIMENSIONS {

            private final byte[] VALUE_SYMBOLS = new byte[] {'=', '"', ','};

            private final WriteInfo VALUE_START = new WriteInfo(VALUE_SYMBOLS, 2, 0);
            private final WriteInfo VALUE_END = new WriteInfo(VALUE_SYMBOLS, 1, 1);
            private final WriteInfo VALUE_END_COMMA = new WriteInfo(VALUE_SYMBOLS, 2, 1);

            @Override
            public Iterator<WriteInfo> serialise(MetricRepresentation context, MetricRepresentation.Value value) {
                return new ExpandingIterator<>(
                        value.dimensions.iterator(),
                        (vl,ctx) -> Arrays.asList(
                                new WriteInfo(safeName(vl.header.name).getBytes(EXPOSITION_CHARSET)),
                                VALUE_START,
                                new WriteInfo(escapeLabelValue(vl.value).getBytes(EXPOSITION_CHARSET)),
                                ctx.hasNext() ? VALUE_END_COMMA : VALUE_END
                        ).iterator()
                );
            }
        },
        DIMENSIONS_END {
            private final WriteInfo DIMENSIONS_END = new WriteInfo(new byte[]{' ', '}', ' '});

            @Override
            public Iterator<WriteInfo> serialise(MetricRepresentation context, MetricRepresentation.Value value) {
                return Iterators.singletonIterator(DIMENSIONS_END);
            }
        },
        VALUE {
            private final WriteInfo P_INF = new WriteInfo(new byte[] {'+', 'I', 'n', 'f'});
            private final WriteInfo N_INF = new WriteInfo(new byte[] {'-', 'I', 'n', 'f'});
            private final WriteInfo NAN = new WriteInfo(new byte[] {'N', 'a', 'n'});

            @Override
            public Iterator<WriteInfo> serialise(MetricRepresentation context, MetricRepresentation.Value value) {
                WriteInfo info;
                if (Double.isNaN(value.value)) {
                    info = NAN;
                } else if (value.value == Double.NEGATIVE_INFINITY){
                    info = N_INF;
                } else if (value.value == Double.POSITIVE_INFINITY) {
                    info =P_INF;
                } else {
                    info = new WriteInfo(Double.toString(value.value).getBytes(EXPOSITION_CHARSET));
                }
                return Iterators.singletonIterator(info);
            }
        },
        //timestamp does not need separator before next term
        TIMESTAMP {
            private final WriteInfo SPACE = new WriteInfo(new byte[]{' '});
            @Override
            public Iterator<WriteInfo> serialise(MetricRepresentation context, MetricRepresentation.Value value) {
                OptionalLong ol = value.timestamp;
                if (!ol.isPresent()) {
                    return Collections.emptyIterator();
                }
                return Arrays.asList(SPACE, new WriteInfo(String.valueOf(ol.getAsLong()).getBytes(EXPOSITION_CHARSET))).iterator();            }
        },
        EOL {
            private final WriteInfo NEWLINE = new WriteInfo(new byte[]{'\n'});

            @Override
            public Iterator<WriteInfo> serialise(MetricRepresentation context, MetricRepresentation.Value value) {
                return Iterators.singletonIterator(NEWLINE);
            }
        };

        public abstract Iterator<WriteInfo> serialise(MetricRepresentation context, MetricRepresentation.Value value);
    }

}
