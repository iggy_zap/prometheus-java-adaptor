/*
This software is designed to act as a prometheus exposition bridge for Java
        Copyright (C) 2018  Ignat Zapolsky

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.izapolsky.prometheus.client;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.function.Consumer;

/**
 * An utility class that transforms metric data from pre-defined metric provider into supplied bytebuffer
 */
public class AssortedMetricsRenderer {

    private final MetricsProvider provider;
    private final MetricWriteTransormer transformer;

    public AssortedMetricsRenderer(MetricsProvider provider, MetricWriteTransormer transformer) {
        this.provider = provider;
        this.transformer = transformer;
    }

    public void renderMetricsTo(OutputStream stream) {
        renderMetrics(it -> {
            try {
                stream.write(it.what, it.position, it.length);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public void renderMetricsTo(ByteBuffer buf) {
        renderMetrics(it -> buf.put(it.what, it.position, it.length));
    }

    protected void renderMetrics (Consumer<MetricRepresentationTransformer.WriteInfo> dataConsumer) {
        for (MetricRepresentation representation : provider) {
            for (MetricRepresentationTransformer.WriteInfo info : transformer.bytes(representation)) {
                dataConsumer.accept(info);
            }
        }
    }


}
