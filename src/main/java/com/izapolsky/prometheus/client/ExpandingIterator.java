/*
This software is designed to act as a prometheus exposition bridge for Java
        Copyright (C) 2018  Ignat Zapolsky

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.izapolsky.prometheus.client;

import com.google.common.collect.AbstractIterator;

import java.util.Iterator;
import java.util.function.BiFunction;

/**
 * An iterator that expands from supplied iterator via provided contextual expansion function
 * @param <EXPANSION>
 * @param <VALUE>
 */
public class ExpandingIterator<EXPANSION, VALUE> extends AbstractIterator<VALUE> {
    private final Iterator<EXPANSION> seeder;
    private final BiFunction<EXPANSION, Iterator<EXPANSION>, Iterator<VALUE>> expander;
    private Iterator<VALUE> expanded;

    /**
     * A constructor to generate stream / iterator of data
     * @param seeder An iterator that provides values for expansions
     * @param expander A bi-function that knows how to expand value of EXPANSION into Iterator&lt;VALUE&gt; It also provides
     *                 expansion context which is seeder iterator itself
     */
    public ExpandingIterator(Iterator<EXPANSION> seeder, BiFunction<EXPANSION, Iterator<EXPANSION>, Iterator<VALUE>> expander) {
        this.seeder = seeder;
        this.expander = expander;
    }

    @Override
    protected VALUE computeNext() {
        do {
            if (expanded == null && seeder.hasNext()) {
                expanded = expander.apply(seeder.next(), seeder);
            }
            if (expanded.hasNext()) {
                return expanded.next();
            } else {
                expanded = null;
            }
        } while (expanded == null && seeder.hasNext());

        return endOfData();
    }
}
